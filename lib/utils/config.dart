import 'package:flutter/material.dart';

class Config {


  static const String supportTrainings = 'Trainings';
  static const String supportHighlights = 'Highlights';
  static const String supportPortfolio = 'Portfolio Value';
  static const String supportPortfolioValue = '₹ 20,48,455';



  static const String supportSortandFilters = 'Sort and Filters';
  static const String supportSortby = 'Sort by';
  static const String supportLocation = 'Location';
  static const String supportTrainingName = 'Training Name';
  static const String supportTrainer = 'Trainer';



  static const String supportWestDesMoines = 'West Des Moines';
  static const String supportChicagoIL = 'Chicago, IL';
  static const String supportPhoenixAZn = 'Phoenix, AZ';
  static const String supportDallsTX = 'Dalls, TX';
  static const String supportSanDiegoCA = 'San Diego, CA';
  static const String supportSanFransiscoCA = 'San Fransisco, CA';
  static const String supportNewYorkZK = 'New York, ZK';
  static const String supportSearch = 'Search';


  static const String supportGain = 'Gain / Loss';
  static const String supportGainValue = '₹ 30,038.20';
  static const String supportGainValueAdd = '+20.9%';

  static const String supportInvestment = 'Investment';
  static const String supportFavorite = 'Favorite';

  static const String supportPriceAlert = 'PRICE ALERT';
  static const String supportfavorite = 'FAVORITE';


  static const String supportProtfolio = 'Protfolio';
  static const String supportWallet = 'Wallet';
  static const String supportPrices = 'Prices';
  static const String supportNews = 'News';
  static const String supportBuy = 'Buy';


  static const String supportBuyPrice = 'Buy Price';
  static const String supportQuantity = 'Quantity';
  static const String supportCURRENT = 'CURRENT';
  static const String supportCrupess = '₹ 1,30,000.52';
  static const String supportCrupessto = '₹ 4161172.84';
  static const String supportBalance = 'Balance';
  static const String supportTotalAmount = 'Total Amount';
  static const String supportETH = 'ETH';


  static const String supportDone = 'Done';
  static const String supportCompleted = 'Completed';
  static const iOSAppID = '000000';



  //SOCIAL LINKS
  static const String facebookPageUrl = 'https://www.facebook.com/mrblab24';
  static const String youtubeChannelUrl = 'https://www.youtube.com/channel/UCnNr2eppWVVo-NpRIy1ra7A';
  static const String twitterUrl = 'https://twitter.com/FlutterDev';



  //app theme color
  final Color appThemeColor = Color(0xFFf2f7fd);



  // Icons
  static const String appIcon = 'assets/images/icon.png';
  static const String logo = 'assets/images/logo.png';
  static const String splash = 'assets/images/splash.png';



  //languages
  static const List<String> languages = [
    'English',
    'Spanish',
    'Arabic'
  ];



  //Image Assets
  static const String commentImage = "assets/images/comment.svg";
  static const String bookmarkImage = "assets/images/bookmark.svg";
  static const String notificationImage = "assets/images/notification.svg";
  static const String noContentImage = "assets/images/no_content.svg";



  //animation files
  static const String doneAnimation = 'assets/animation_files/done.flr';
  static const String searchAnimation = 'assets/animation_files/search.flr';


}
