import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task/model/list_view_model.dart';
import 'package:flutter_task/utils/config.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int page =0;
  PageController pageController = PageController(initialPage: 0);

  void nextPage(){
    if(page == 0){
      page = 1;
    }else if(page == 1){
      page = 2;
    }else if(page == 2){
      page = 3;
    }else if(page == 3){
      page = 4;
    }else if(page == 4){
      page = 5;
    }else if(page == 5){
      page = 6;
    }else if(page == 6){
      page = 7;
    }else if(page == 7){
      page = 0;
    }
    pageController.animateToPage(page,
        duration: Duration(milliseconds: 400),
        curve: Curves.easeIn
    );
  }

  void previousPage(){
    if(page == 0){
      page = 7;
    }else if(page == 1){
      page = 0;
    }else if(page == 2){
      page = 1;
    }else if(page == 3){
      page = 2;
    }else if(page == 4){
      page = 3;
    }else if(page == 5){
      page = 4;
    }else if(page == 6){
      page = 5;
    }else if(page == 7){
      page = 6;
    }
    pageController.animateToPage(page,
        duration: Duration(milliseconds: 400),
        curve: Curves.easeIn
    );
  }
  int checkselect = 1;
  List<Widget> _list = <Widget>[
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 1",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 2",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 3",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 4",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 5",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 6",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
    new Container(color: Colors.blue,width: 250,height: 100,child: new Center(child: new Text("Page 7",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),),),
  ];

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }
  List<bool> checkbbol = [true,false,false,false,false,false,false,false,false];
  double heightOfModalBottomSheet = 0.0;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      endDrawer: Drawer(),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          Config.supportTrainings,
          style: TextStyle(
              fontFamily: "Manrope-Bold",
              fontWeight: FontWeight.w700,
              color: Colors.white,
              fontSize: 30,
              letterSpacing: 1.0),
        ),
        elevation: 0,
        backgroundColor: Color(0xffff4855),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.20,
                    color: Color(0xffff4855),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.68,
                    color: Colors.transparent,
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: Stack(
                            children: [
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet<void>(
                                    context: context,
                                    builder: (BuildContext context) {


                                      return StatefulBuilder(builder: (BuildContext context, StateSetter setState /*You can rename this!*/) {
                                        return Container(
                                          height: 530 +heightOfModalBottomSheet,
                                          color: const Color(0xFF737373),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Theme.of(context).canvasColor,
                                            ),
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 5),
                                                    width: MediaQuery.of(context).size.width,
                                                    height: 60,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Text(Config.supportSortandFilters,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 25),),
                                                        InkWell(onTap: (){
                                                          Navigator.pop(context);
                                                        },child: Icon(Icons.clear,color: Colors.black,))
                                                      ],),
                                                  ),
                                                  Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    color: Color(0xffdadee0),
                                                    height: 1,
                                                  ),
                                                  Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    height: 370,
                                                    child: Row(
                                                      children: [
                                                        Expanded(flex: 04,child: Column(mainAxisAlignment: MainAxisAlignment.start,
                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                          children: [


                                                            InkWell(
                                                              onTap: (){
                                                                setState(() {
                                                                  heightOfModalBottomSheet +=10;
                                                                  checkselect =0;
                                                                });
                                                              },
                                                              child: Container(height: 60,color: checkselect ==0?Colors.transparent : Colors.grey[400],child: checkselect ==0? Row(children: [
                                                                Expanded(flex: 05,child: Container(width: 5,height: 60,color: Colors.red,),),
                                                                Expanded(flex: 95,child: Center(child: Text(Config.supportSortby,style: TextStyle(color: checkselect ==0?Colors.black : Colors.white,fontWeight: checkselect ==0? FontWeight.bold:  FontWeight.normal),),)),


                                                              ],):Center(child: Text(Config.supportSortby,style: TextStyle(color: checkselect ==0?Colors.black : Colors.white,fontWeight: checkselect ==0? FontWeight.bold:  FontWeight.normal),),),),
                                                            ),




                                                            InkWell(
                                                              onTap: (){
                                                                setState(() {
                                                                  checkselect =1;
                                                                });
                                                              },
                                                              child:  Container(height: 60,color: checkselect ==1?Colors.transparent : Colors.grey[400],child: checkselect ==1? Row(children: [
                                                                Expanded(flex: 05,child: Container(width: 5,height: 60,color: Colors.red,),),
                                                                Expanded(flex: 95,child: Center(child: Text(Config.supportLocation,style: TextStyle(color: checkselect ==1?Colors.black : Colors.white,fontWeight: checkselect ==1? FontWeight.bold:  FontWeight.normal),),)),


                                                              ],):Center(child: Text(Config.supportLocation,style: TextStyle(color: checkselect ==1?Colors.black : Colors.white,fontWeight: checkselect ==1? FontWeight.bold:  FontWeight.normal),),),),
                                                            ),



                                                            InkWell(
                                                              onTap: (){
                                                                setState(() {
                                                                  checkselect =2;
                                                                });
                                                              },
                                                              child:   Container(height: 60,color: checkselect ==2?Colors.transparent : Colors.grey[400],child: checkselect ==2? Row(children: [
                                                                Expanded(flex: 05,child: Container(width: 5,height: 60,color: Colors.red,),),
                                                                Expanded(flex: 95,child: Center(child: Text(Config.supportTrainingName,style: TextStyle(color: checkselect ==2?Colors.black : Colors.white,fontWeight: checkselect ==2? FontWeight.bold:  FontWeight.normal),),)),


                                                              ],):Center(child: Text(Config.supportTrainingName,style: TextStyle(color: checkselect ==2?Colors.black : Colors.white,fontWeight: checkselect ==2? FontWeight.bold:  FontWeight.normal),),),),
                                                            ),





                                                            InkWell(
                                                              onTap: (){
                                                                setState(() {
                                                                  checkselect =3;
                                                                });
                                                              },
                                                              child:   Container(height: 60,color: checkselect ==3?Colors.transparent : Colors.grey[400],child: checkselect ==3? Row(children: [
                                                                Expanded(flex: 05,child: Container(width: 5,height: 60,color: Colors.red,),),
                                                                Expanded(flex: 95,child: Center(child: Text(Config.supportTrainingName,style: TextStyle(color: checkselect ==3?Colors.black : Colors.white,fontWeight: checkselect ==3? FontWeight.bold:  FontWeight.normal),),)),


                                                              ],):Center(child: Text(Config.supportTrainer,style: TextStyle(color: checkselect ==3?Colors.black : Colors.white,fontWeight: checkselect ==3? FontWeight.bold:  FontWeight.normal),),),),
                                                            ),







                                                          ],)),
                                                        Expanded(flex: 06,child: Column(mainAxisAlignment: MainAxisAlignment.start,
                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                          children: [
                                                            SizedBox(width: 10),
                                                            Container(
                                                              margin: const EdgeInsets.symmetric(horizontal: 5),
                                                              child: TextField(
                                                                keyboardType: TextInputType.text,
                                                                autofocus: false,
                                                                style: TextStyle(color: Colors.blue),
                                                                obscureText: false,
                                                                decoration: const InputDecoration(
                                                                  enabledBorder: const OutlineInputBorder(
                                                                    borderSide:
                                                                    const BorderSide(color: Colors.red, width: 0.0),
                                                                  ),
                                                                  focusedBorder: const OutlineInputBorder(
                                                                    borderSide:
                                                                    const BorderSide(color: Colors.black, width: 0.0),
                                                                  ),
                                                                  border: OutlineInputBorder(borderSide: BorderSide(color: Colors.black, width: 1.0),),
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  hintText: 'Search',
                                                                  hintStyle: TextStyle(color: Colors.black),),

                                                              ),
                                                            ),

                                                            SizedBox(width: 10), //SizedBox
                                                            /** Checkbox Widget **/
                                                            CheckboxListTile(
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              title: Text(Config.supportWestDesMoines,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[0] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[0],
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[0] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),
                                                            CheckboxListTile(
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              title: Text(Config.supportChicagoIL,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[1] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[1],
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[1] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),
                                                            CheckboxListTile(
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              title: Text(Config.supportPhoenixAZn,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[2] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[2],
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[2] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),
                                                            CheckboxListTile(
                                                              title: Text(Config.supportDallsTX,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[3] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[3],
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[3] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),
                                                            CheckboxListTile(
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              title: Text(Config.supportSanDiegoCA,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[4] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[4],
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[4] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),
                                                            CheckboxListTile(
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              title: Text(Config.supportSanFransiscoCA,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[5] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[5],
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[5] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),

                                                            CheckboxListTile(
                                                              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                                                              title: Text(Config.supportNewYorkZK,style: TextStyle(color: Colors.black,fontWeight:  checkbbol[6] ? FontWeight.bold:FontWeight.normal),),
                                                              value: checkbbol[6],
                                                              contentPadding: EdgeInsets.zero,
                                                              onChanged: (newValue) {
                                                                setState(() {
                                                                  this.checkbbol[6] = newValue!;
                                                                });
                                                              },
                                                              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                                            ),
                                                          ],)),
                                                      ],),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      });

                                    },
                                  );
                                },
                                child: Container(
                                  width: 70,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  margin: const EdgeInsets.fromLTRB(
                                      20.0, 70.0, 0.0, 20.0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        width: 1, color: Colors.grey),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                  ),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.filter_list_rounded,
                                        color: Colors.grey,
                                        size: 15,
                                      ),
                                      Text(
                                        "Filter",
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          child: ListView.builder(
                              itemCount: CoinListDataModel.flutteritems.length,
                              itemBuilder: (context, int index) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  height: 175,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                    boxShadow: [
                                      BoxShadow(
                                        offset: Offset(2, 2),
                                        color: Colors.grey,
                                        blurRadius: 10.0,
                                      ),
                                      BoxShadow(
                                        offset: Offset(-2, -2),
                                        color: Colors.white,
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          flex: 04,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      CoinListDataModel
                                                          .flutteritems[index]
                                                          .date,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 20),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      CoinListDataModel
                                                          .flutteritems[index]
                                                          .time,
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xff797879),
                                                          fontSize: 9),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      CoinListDataModel
                                                          .flutteritems[index]
                                                          .title,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 11),
                                                    ),
                                                    Text(
                                                      CoinListDataModel
                                                          .flutteritems[index]
                                                          .subtitle,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 11),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )),
                                      Expanded(
                                          flex: 06,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      CoinListDataModel
                                                          .flutteritems[index]
                                                          .tag,
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xffff4855),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 10),
                                                    ),
                                                    Text(
                                                      CoinListDataModel
                                                          .flutteritems[index]
                                                          .tagtitle,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 15),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets
                                                              .fromLTRB(
                                                          0.0, 10.0, 0.0, 0.0),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            width: 40,
                                                            height: 40,
                                                            decoration: BoxDecoration(
                                                                boxShadow: [
                                                                  BoxShadow(
                                                                    offset:
                                                                        Offset(
                                                                            1,
                                                                            1),
                                                                    color: Colors
                                                                        .grey,
                                                                    blurRadius:
                                                                        5.0,
                                                                  ),
                                                                  BoxShadow(
                                                                    offset:
                                                                        Offset(
                                                                            -1,
                                                                            -1),
                                                                    color: Colors
                                                                        .white,
                                                                    blurRadius:
                                                                        5.0,
                                                                  ),
                                                                ],
                                                                shape: BoxShape
                                                                    .circle,
                                                                image: DecorationImage(
                                                                    image: NetworkImage(CoinListDataModel
                                                                        .flutteritems[
                                                                            index]
                                                                        .image),
                                                                    fit: BoxFit
                                                                        .cover)),
                                                          ),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                CoinListDataModel
                                                                    .flutteritems[
                                                                        index]
                                                                    .username,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        11),
                                                              ),
                                                              Text(
                                                                CoinListDataModel
                                                                    .flutteritems[
                                                                        index]
                                                                    .discription,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontSize:
                                                                        11),
                                                              ),
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(10),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      "",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 11),
                                                    ),
                                                    ElevatedButton(
                                                        onPressed: () {},
                                                        child: Text(
                                                          "Enrol Now",
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ))
                                                  ],
                                                ),
                                              )
                                            ],
                                          ))
                                    ],
                                  ),
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Text(
                          Config.supportHighlights,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              letterSpacing: 1.0),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 75),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(onTap: (){
                          nextPage();
                        },child: Container(width: 30,height: 90,color: Color(0x80000000),alignment: Alignment.center,child: Icon(Icons.arrow_back_ios,color: Colors.white,size: 15,),)),
                        Container(width: 300,height: 130,
                        child: PageView(
                          children: _list,
                          reverse: true,
                          physics: BouncingScrollPhysics(),
                          controller:pageController,
                          onPageChanged: (num){

                          },
                        ),),
                        InkWell(onTap: (){
                          previousPage();
                        },child: Container(width: 30,height: 90,color: Color(0x80000000),alignment: Alignment.center,child: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 15),)),
                      ],
                    ),
                  ),

                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
