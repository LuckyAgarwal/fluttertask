import 'package:flutter/material.dart';
import 'package:flutter_task/screen/home.dart';
import 'package:flutter_task/utils/ThemeModel.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeModel().lightTheme,
      darkTheme: ThemeModel().darkTheme,
      home: HomeScreen(),
    );
  }
}
