class CoinListDataModel{
  static final flutteritems = [
    FlutterCoinItem(
        id: 1,
        title: 'Convention Hall, ',
        subtitle: 'Greater Des Moines',
        tag: "Filling Fast",
        tagtitle: "Safe Scrum Master (4.6)",
        username: "Keynote Speaker",
        discription: "Helen Gribble",
        date: 'Oct 11-13, 2019',
        time: "08:30 am - 12:30 pm",
        status: '1',
        isNew: true,
        image: 'https://media.istockphoto.com/photos/smiling-indian-man-looking-at-camera-picture-id1270067126?k=20&m=1270067126&s=612x612&w=0&h=ZMo10u07vCX6EWJbVp27c7jnnXM2z-VXLd-4maGePqc=',
        balance: '₹ 8,789',
        profit: '2.1',
        coin_price:'₹ 41,99,210',
        name_short:'BTC',
        market_Cap: '(+1.10%)',
        icon: 'assets/images/bitcoin.png'),

    FlutterCoinItem(
        id: 2,
        title: 'Convention Hall, ',
        subtitle: 'Greater Des Moines',
        tag: "Filling Fast",
        tagtitle: "Safe Scrum Master (4.6)",
        username: "Keynote Speaker",
        discription: "Helen Gribble",
        date: 'Oct 11-13, 2019',
        time: "08:30 am - 12:30 pm",
        status: '1',
        isNew: true,
        image: 'https://media.gettyimages.com/photos/handsome-young-adult-businessman-with-stubble-picture-id1250238624?s=612x612',
        balance: '₹ 8,789',
        profit: '2.1',
        coin_price:'₹ 41,99,210',
        name_short:'BTC',
        market_Cap: '(+1.10%)',
        icon: 'assets/images/bitcoin.png'),


  ];
}



class FlutterCoinItem {
  final int id;
  final String title;
  final String subtitle;
  final String tag;
  final String tagtitle;
  final String username;
  final String discription;
  final String date;
  final String time;
  final String status;
  final bool isNew;
  final String image;
  final String balance;
  final String profit;
  final String coin_price;
  final String name_short;
  final String market_Cap;
  final String icon;

  FlutterCoinItem({
    required this.id,
    required this.title,
    required this.subtitle,
    required this.tag,
    required this.tagtitle,
    required this.username,
    required this.discription,
    required this.date,
    required this.time,
    required this.status,
    required this.isNew,
    required this.image,
    required this.balance,
    required this.profit,
    required this.coin_price,
    required this.name_short,
    required this.market_Cap,
    required this.icon,
  });
}